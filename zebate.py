# Opis szyfru:
# https://mattomatti.com/pl/a35ae

from collections import deque
from typing import Deque, List
from dataclasses import dataclass
from random import randint, sample, shuffle


@dataclass
class Key:
    cogs_count: int
    cogs_teeth_count: List[int]
    cogs: List[Deque[str | None]]


def encrypt(plaintext: str, key: Key) -> str:
    ciphertext = []
    for char in plaintext:
        for cog_index, cog in enumerate(key.cogs):
            if char in cog:
                char_index = cog.index(char)
                enc = f"{cog_index + 1}{char_index}"
                ciphertext.append(enc)
                key = rotate_cogs(key, cog_index)
                break
        else:
            ciphertext.append("00")
    return "".join(ciphertext)


def decrypt(ciphertext: str, key: Key) -> str:
    plaintext = []
    i = 0
    while i < len(ciphertext):
        try:
            if ciphertext[i] == "0":
                plaintext.append(ciphertext[i + 1])
                i += 2
            else:
                cog_index = int(ciphertext[i]) - 1
                char_index = int(ciphertext[i + 1])
                cog = key.cogs[cog_index]
                plaintext.append(cog[char_index])
                key = rotate_cogs(key, cog_index)
                i += 2
        except IndexError as e:
            print(
                f"Error: Failed to decrypt '{ciphertext[i + 1]}' in ciphertext at position {i} using cog {cog}({cog_index=})."
            )
            raise e
    return "".join(plaintext)


def _clamp(x: int, min_val: int, max_val: int) -> int:
    return max(min_val, min(max_val, x))


def generate_key(
    alpha: str, min_empty_spaces: int = 0, cogs_teeth_count: List[int] | None = None
) -> Key:
    alpha_length = len(alpha)
    letters = set(alpha)
    key = Key(0, [], [])

    if cogs_teeth_count is not None:
        if sum(cogs_teeth_count) < alpha_length:
            raise ValueError(
                f"Sum of cogs_teeth_count ({sum(cogs_teeth_count)}) must be greater or equal to length of alphabet ({alpha_length})"
            )
        key.cogs_teeth_count = cogs_teeth_count
    else:
        while sum(key.cogs_teeth_count) < alpha_length:
            key.cogs_teeth_count.append(
                randint(
                    2,
                    _clamp(
                        alpha_length - sum(key.cogs_teeth_count),
                        2,
                        min(alpha_length // 2, 9),
                    ),
                )
            )

    key.cogs_count = len(key.cogs_teeth_count)
    key.cogs = []

    for i in range(key.cogs_count):
        new_cog: List[str | None] = []

        # Fill the cog with letters from the alphabet
        for _ in range(key.cogs_teeth_count[i]):
            if letters:
                new_cog.append(letters.pop())
            else:
                new_cog.append(None)

        # Add empty spaces if necessary
        for _ in range(max(0, min_empty_spaces - len(new_cog))):
            new_cog.append(None)

        # Shuffle the cog randomly
        shuffle(new_cog)

        key.cogs.append(deque(new_cog))

    return key


def copy_key(key: Key) -> Key:
    return Key(
        key.cogs_count, key.cogs_teeth_count.copy(), [cog.copy() for cog in key.cogs]
    )


def rotate_cogs(key: Key, num_steps: int = 1, reverse: bool = False) -> Key:
    new_key = copy_key(key)
    for position, cog in enumerate(new_key.cogs):
        if reverse:
            cog.rotate(-num_steps)
        else:
            cog.rotate(num_steps)
    return new_key
