#!/usr/bin/env python

import json
from os import listdir
from typing import List, Union
from dataclasses import dataclass
from zebate import encrypt, decrypt, generate_key, rotate_cogs
from string import ascii_lowercase
from ngram_score import ngram_score


@dataclass
class BookData:
    source: str
    text: str
    name: str


def display_texts_and_get_choice(texts: List[BookData]) -> Union[str, BookData]:
    """
    This function displays the texts and gets the user's choice.

    Parameters:
    texts (list): A list of BookData objects.

    Returns:
    text (str or object): The chosen text, either from the list or input by the user.
    """
    for i, text in enumerate(texts):
        print(f"{i + 1}. {text.name}")
        print(f"Source: {text.source}")
        print(f"Text: {text.text}")
        print("\n\n")

    print("Choose the text by typing the number of the text or provide your own text")
    choice = input("Your choice: ")
    try:
        choice = int(choice)
        text = texts[choice - 1]
        print(f"You have chosen the text: {text.name}")
        print(f"Source: {text.source}")
        print(f"Text: {text.text}")
    except ValueError:
        text = str(choice)
        print(f"You have chosen the text: {text}")

    return text


alphabets = {
    "en": ascii_lowercase,
    "es": "aábcdeéfghiíjklmnñoópqrstuúüvwxyz",
    "pl": "aąbcćdeęfghijklłmnńoópqrsśtuvwxyzźż",
}


def detect_alphabet(text: str) -> str:
    """
    This function detects the alphabet of a given string.
    """

    for alpha in alphabets:
        if all(char in alphabets[alpha] for char in text):
            return alpha

    print("Cannot detect alphabet. Assuming English")
    return "en"


def clean_text(text: str) -> str:
    return "".join(char for char in text if char.isalpha()).lower()


def main():
    # read dir text_examples and extract all json files,

    texts: List[BookData] = []
    for file in listdir("text_examples"):
        if file.endswith(".json"):
            with open(f"text_examples/{file}", "r", encoding="utf-8") as f:
                data = json.load(f)
                texts.append(BookData(**data))

    book_data_or_str = display_texts_and_get_choice(texts)

    text = clean_text(
        book_data_or_str.text
        if isinstance(book_data_or_str, BookData)
        else str(book_data_or_str)
    )

    detected_alpha = detect_alphabet(text)
    alpha = alphabets[detected_alpha]
    print("Detected alphabet: ", alpha)
    key = generate_key(alpha)
    print("Generated key: ", key)
    print("Number of cogs: ", key.cogs_count)
    print("Teeth count per cog: ", key.cogs_teeth_count)
    print("Generated key: ", key)

    print("Original text: ", text)
    encrypted_text = encrypt(text, key)
    print("Encrypted text: ", encrypted_text)

    print("Text to decrypt: ", encrypted_text)
    decrypted_text = decrypt(encrypted_text, key)
    print("Decrypted text: ", decrypted_text)

    scorer = ngram_score("en.txt")

    print(f"{scorer.score(text) = }")

    print(f"{scorer.score(encrypted_text) =}")


if __name__ == "__main__":
    main()
